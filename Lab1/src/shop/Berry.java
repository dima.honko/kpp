package shop;

public class Berry extends Fruit{
    private boolean isStone;
    private boolean isForest;

    public Berry(String color, String name, double weight, double price, Seasons harvest, boolean isStone, boolean isForest) {
        super(color, name, weight, price, harvest);
        this.isStone = isStone;
        this.isForest = isForest;
    }

    public boolean isStone() {
        return isStone;
    }

    public void setStone(boolean stone) {
        isStone = stone;
    }

    public boolean isForest() {
        return isForest;
    }

    public void setForest(boolean forest) {
        isForest = forest;
    }
}
