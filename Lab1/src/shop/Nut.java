package shop;

public class Nut extends Fruit{
    boolean isShell;
    boolean isForest;

    public Nut(String color, String name, double weight, double price, Seasons harvest, boolean isShell, boolean isForest) {
        super(color, name, weight, price, harvest);
        this.isShell = isShell;
        this.isForest = isForest;
    }

    public boolean isShell() {
        return isShell;
    }

    public void setShell(boolean shell) {
        isShell = shell;
    }

    public boolean isForest() {
        return isForest;
    }

    public void setForest(boolean forest) {
        isForest = forest;
    }
}
