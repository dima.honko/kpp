package shop;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Fruit> list = new ArrayList<>();

        list.add(new Berry("Red","Strawberry",0.5,50,Seasons.SUMMER,false,false));
        list.add(new Berry("Red","Raspberry",0.3,40,Seasons.SUMMER,false,false));
        list.add(new Berry("Red","Cerry",1,60,Seasons.SUMMER,true,false));
        list.add(new Berry("Blue","Blueberry",0.5,70,Seasons.SUMMER,false,true));
        list.add(new Citrus("Orange","Orange",0.5,20,Seasons.WINTER,true,false));
        list.add(new Citrus("Orange","Мандарин",0.5,34,Seasons.WINTER,true,false));
        list.add(new Citrus("Yellow","Lemon",0.5,21,Seasons.SUMMER,false,true));
        list.add(new Nut("Brown","Blueberry",0.5,67,Seasons.SUMMER,false,false));
        list.add(new Nut("White","Peanut",0.5,90,Seasons.SUMMER,false,false));
        list.add(new Nut("Brown","Ліщина",0.5,54,Seasons.SUMMER,true,true));

        FruitsManager maneger = new FruitsManager();
        System.out.println("До сортування:");
        display(list);
        maneger.sortByClor(list);
        System.out.println("Після сортування");
        display(list);
        maneger.sortByClorDec(list);
        System.out.println("Після сортування");
        display(list);
        maneger.sortByPrice(list);
        System.out.println("Після сортування");
        display(list);
        maneger.sortByPriceDec(list);
        System.out.println("Після сортування");
        display(list);
        System.out.println("Після пошуку");
        display(maneger.findSummer(list));



    }
    static void display (ArrayList<Fruit> list) {
        for (Fruit fruit : list) {
            System.out.println(fruit);
        }
    }

}
