package shop;

public class Citrus extends Fruit {
    private boolean isImported;
    private boolean isSour;

    public Citrus(String color, String name, double weight, double price, Seasons harvest, boolean isImported, boolean isSour) {
        super(color, name, weight, price, harvest);
        this.isImported = isImported;
        this.isSour = isSour;
    }

    public boolean isImported() {
        return isImported;
    }

    public void setImported(boolean imported) {
        isImported = imported;
    }

    public boolean isSour() {
        return isSour;
    }

    public void setSour(boolean sour) {
        isSour = sour;
    }

}
