package shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FruitsManager {
    public void sortByClor (ArrayList<Fruit> list) {
        Collections.sort(list,new Fruit.ComparatorByColor());
    }
    public void sortByPrice (ArrayList<Fruit> list) {
        Collections.sort(list,list.get(0).new ComparatorByPrice());
    }
    public void sortByPriceDec (ArrayList<Fruit> list) {
        Collections.sort(list,new Comparator<Fruit>(){
            @Override
            public int compare(Fruit o1, Fruit o2) {
                return (int) ( o2.getPrice() - o1.getPrice() ) * 100;
            }
        });
    }
    public void sortByClorDec (ArrayList<Fruit> list) {
        Collections.sort(list,(Fruit o1, Fruit o2)->o2.getColor().compareTo(o1.getColor()));
    }
    public ArrayList<Fruit> findSummer (ArrayList<Fruit> list) {
        ArrayList<Fruit> res = new ArrayList<>();
        for (Fruit fruit : list) {
            if (fruit instanceof Berry || fruit instanceof Citrus) {
                if (fruit.getHarvest() ==Seasons.SUMMER) {
                    res.add(fruit);
                }
            }
        }
        return res;
    }
}
