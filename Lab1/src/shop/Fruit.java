package shop;

import java.util.Comparator;
import java.util.Date;

public class Fruit {

    private String color;
    private String name;
    private double weight;
    private double price;
    private Seasons harvest;

    static class ComparatorByColor implements Comparator<Fruit> {

        @Override
        public int compare(Fruit o1, Fruit o2) {
            return o1.color.compareTo(o2.color);
        }
    }

    class ComparatorByPrice implements Comparator<Fruit> {

        @Override
        public int compare(Fruit o1, Fruit o2) {
            return (int) (o1.price - o2.price)*100;
        }
    }
    public Fruit(String color, String name, double weight, double price,Seasons harvest) {
        this.color = color;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.harvest = harvest;
    }

    public Fruit() {

    }

    public Seasons getHarvest() {
        return harvest;
    }

    public void setHarvest(Seasons harvest) {
        this.harvest = harvest;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "color= " + color +
                ", name= " + name +
                ", weight= " + weight +
                ", price= " + price +
                ", harvest= " + harvest;
    }
}
