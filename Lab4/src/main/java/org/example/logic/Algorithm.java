package org.example.logic;

import java.util.*;

public class Algorithm implements Runnable
{
    private Graph graph;
    private Node source;
    private  PriorityQueue<Node> queue;

    public Algorithm(Graph graph)
    {
        this.graph = graph;
        this.source = graph.getNode(2);
        this.source.minDistance = 0;
        this.queue = new PriorityQueue<>();
        this.queue.add(source);
    }

    public Graph getGraph()
    {
        return graph;
    }

    public void setGraph(Graph graph)
    {
        this.graph = graph;
    }

    public Node getSource()
    {
        return this.source;
    }

    public void setSource(Node source)
    {
        this.source = source;
        this.queue = new PriorityQueue<>();
        this.queue.add(source);
    }


    public synchronized void execute() {
        while(!queue.isEmpty())
        {
            Node n = queue.poll();

            for(Edge neighbour : n.neighbours)
            {
                double newDist = n.minDistance + neighbour.getWeight();
                if(neighbour.getTarget().minDistance>newDist)
                {
                    queue.remove(neighbour.getTarget());
                    neighbour.getTarget().minDistance = newDist;
                    neighbour.getTarget().path = new LinkedList<>(n.path);
                    neighbour.getTarget().path.add(n);
                    queue.add(neighbour.getTarget());
                }
            }
        }
    }

    @Override
    public void run()
    {
        execute();
    }
}

