package org.example.logic;

import java.util.*;

public class Node implements Comparable<Node>
{
    private final String name;
    public ArrayList<Edge> neighbours;
    public LinkedList<Node> path;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Node previous;

    public Node(String name)
    {
        this.name = name;
        neighbours = new ArrayList<Edge>();
        path = new LinkedList<Node>();
    }

    public String toString()
    {
        return name;
    }

    public String getInfo() {
        return "Node - " + name + ","
                + "Min distance=" + minDistance + ","
                + "Path=" + path.toString() + "\n";
    }

    @Override
    public int compareTo(Node other)
    {
        return Double.compare(minDistance,other.minDistance);
    }
}

