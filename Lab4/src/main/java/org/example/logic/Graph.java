package org.example.logic;

import java.util.*;

public class Graph
{
    private ArrayList<Node> nodes;

    public Graph(int nodesCount)
    {
        nodes = new ArrayList<Node>(nodesCount);
        for(int i = 0; i < nodesCount;i++)
        {
            nodes.add(new Node("#" + i));
        }
    }

    public void addEdge(int src, int dest, int weight)
    {
        Node s = nodes.get(src);
        Edge new_edge = new Edge(nodes.get(dest),weight);
        s.neighbours.add(new_edge);
    }

    public ArrayList<Node> getNodes()
    {
        return nodes;
    }

    public Node getNode(int id)
    {
        return nodes.get(id);
    }

}

