package org.example.logic;

import java.util.*;

public class ThreadMonitor
{
    private ArrayList<Thread> threads;

    public ThreadMonitor()
    {
        threads = new ArrayList<Thread>();
    }

    public void addThread(Thread t)
    {
        threads.add(t);
    }

    public void removeThread(int id)
    {
        threads.remove(id);
    }

    public String getThreadInfo(int i)
    {
        return "\n" + "Name: " + threads.get(i).getName() + "\n" +
                "Id: " + threads.get(i).getId() + "\n" +
                "State: " + threads.get(i).getState() + "\n" +
                "Priority: " + threads.get(i).getPriority() + "\n";
    }

    public String getThreadInfo(Thread thread)
    {
        return "\n" + "Name: " + thread.getName() + "\n" +
                "Id: " + thread.getId() + "\n" +
                "State: " + thread.getState() + "\n" +
                "Priority: " + thread.getPriority() + "\n";
    }

    public ArrayList<Thread> getThreads()
    {
        return threads;
    }

    public void setThreads(ArrayList<Thread> threads)
    {
        this.threads = threads;
    }
}

