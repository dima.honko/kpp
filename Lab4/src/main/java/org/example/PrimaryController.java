package org.example;

import java.io.IOException;
import java.time.LocalTime;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import org.example.logic.Algorithm;
import org.example.logic.Graph;
import org.example.logic.Node;
import org.example.logic.ThreadMonitor;

public class PrimaryController {

    @FXML
    private TextArea output;

    @FXML
    private void startProgram() {
        Graph graph = new Graph(9);
        graph.addEdge(0, 1, 4); graph.addEdge(0, 7, 8);
        graph.addEdge(1, 2, 8); graph.addEdge(1, 7, 11);
        graph.addEdge(2, 1, 8); graph.addEdge(2, 8, 2);
        graph.addEdge(2, 5, 4); graph.addEdge(2, 3, 7);
        graph.addEdge(3, 2, 7); graph.addEdge(3, 5, 14);
        graph.addEdge(3, 4, 9); graph.addEdge(4, 3, 9);
        graph.addEdge(4, 5, 10); graph.addEdge(5, 4, 10);
        graph.addEdge(5, 3, 9); graph.addEdge(5, 2, 4);
        graph.addEdge(5, 6, 2); graph.addEdge(6, 7, 1);
        graph.addEdge(6, 8, 6); graph.addEdge(6, 5, 2);
        graph.addEdge(7, 0, 8); graph.addEdge(7, 8, 7);
        graph.addEdge(7, 1, 11); graph.addEdge(7, 6, 1);
        graph.addEdge(8, 2, 2); graph.addEdge(8, 7, 7);
        graph.addEdge(8, 6, 6);

        Algorithm algorithm = new Algorithm(graph);
        Thread t1 = new Thread(algorithm);
        Thread t2 = new Thread(algorithm);
        Thread t3 = new Thread(algorithm);
        Thread t4 = new Thread(algorithm);
        ThreadMonitor threadMonitor = new ThreadMonitor();
        threadMonitor.addThread(t1);
        threadMonitor.addThread(t2);
        threadMonitor.addThread(t3);
        threadMonitor.addThread(t4);
        int count = 1;
        for (Thread thread : threadMonitor.getThreads()) {
            System.out.println(threadMonitor.getThreadInfo(thread));
            output.appendText(threadMonitor.getThreadInfo(thread));
        }
        LocalTime start = LocalTime.now();
        for (Thread thread : threadMonitor.getThreads()) {
            thread.start();
            thread.setPriority(count++);
        }
        LocalTime end = LocalTime.now();
        long consumed = end.getNano() - start.getNano();
        consumed /= 80;
        for (Node node : algorithm.getGraph().getNodes()) {
            System.out.println(node.getInfo());
            output.appendText(node.getInfo());
        }
        System.out.println("Time consumed: " + consumed + "ns\n");
        output.appendText("Time consumed: " + consumed + "ns\n");
        algorithm.setSource(graph.getNode(0));
        start = LocalTime.now();
        algorithm.execute();
        end = LocalTime.now();
        consumed = end.getNano() - start.getNano();
        for (Node node : algorithm.getGraph().getNodes()) {
            System.out.println(node.getInfo());
            output.appendText(node.getInfo());
        }
        System.out.println("Time consumed: " + consumed + "ns\n");
        output.appendText("Time consumed: " + consumed + "ns\n");
    }
}
