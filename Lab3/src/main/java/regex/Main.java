package regex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println("Enter length");
        int length = scanner.nextInt();
        delete(text,length);
    }

    static String delete (String text, int length) {
        String[] data = text.split(" ");
        List<String> list = new ArrayList<>(Arrays.asList(data));
        if(length > 1) {
            list.removeIf(datum -> datum.matches("(?i)\\b^[aeiou].{" + (length - 2) + "}[a-z]$"));
        } else {
            list.removeIf(datum -> datum.matches("(?i)\\b^[aeiou]"));
        }
        System.out.println(list);
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s + " ");
        }
        return sb.toString();
    }
}
//asda sdsa sdsa sda asd as a Asds Aaaa sdsadas