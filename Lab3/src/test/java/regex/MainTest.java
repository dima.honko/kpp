package regex;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private String testText = "aa a u asda assa ssss asdas dssa Aaaa sAsa";
    @Test
    void deleteTestGreater1 () {
        assertEquals("aa a u ssss asdas dssa sAsa ",Main.delete(testText,4));
    }
    @Test
    void deleteTest1 () {
        assertEquals("aa asda assa ssss asdas dssa Aaaa sAsa ",Main.delete(testText,1));
    }
}