package corporation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    private static List<Employee> employees = new ArrayList<Employee>();
    public static void main(String[] args) {
        Scanner scanner = null;
        try {
            File file = new File("/Users/Shared/CPP/lab2CPP.txt");
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        while (scanner.hasNextLine()) {
            Employee temp = new Employee();
            String[] data = scanner.nextLine().split(" ");
            temp.setSurname(data[0]);
            temp.setPosition(data[1]);
            temp.setBirthYear(Integer.parseInt(data[2]));
            temp.setSalary(Integer.parseInt(data[3]));
            employees.add(temp);
        }
        if (scanner != null) {
            scanner.close();
        }
        Scanner input = new Scanner(System.in);
        boolean work = true;
        while (work) {
            System.out.println("Enter Action: ");
            int act = input.nextInt();
            switch (act) {
                case 1: task1();
                break;
                case 2: task2();
                break;
                case 3: task3();
                break;
                case 4: work=false;
                break;
                default:
                    System.out.println("Reenter action");
            }
        }
    }
    static void task1() {
        Map<String ,List<Employee>> mapPosition = new HashMap<String ,List<Employee>>();
        for (Employee employee : employees) {
            List<Employee> temp =new ArrayList<Employee>();
            temp.add(employee);
            if (mapPosition.get(employee.getPosition()) != null) {
                temp.addAll(mapPosition.get(employee.getPosition()));
            }
            mapPosition.put(employee.getPosition(),temp);
        }
        for (String s : mapPosition.keySet()) {
            int oldesty = 2020;
            Employee oldest = null;
            int youngesty = 1900;
            Employee youngest = null;
            for (Employee employee : mapPosition.get(s)) {
                if (oldesty > employee.getBirthYear()) {
                    oldesty = employee.getBirthYear();
                    oldest = employee;
                }
                if (youngesty < employee.getBirthYear()) {
                    youngesty = employee.getBirthYear();
                    youngest = employee;
                }
            }
            System.out.println("Oldest in " + s + " is " + oldest);
            System.out.println("Youngest in "+ s + " is " + youngest);
        }
    }
    static void task2() {
        Map<SalaryCategory ,List<Employee>> mapSalary = new HashMap<SalaryCategory ,List<Employee>>();
        for (Employee employee : employees) {
            List<Employee> temp =new ArrayList<Employee>();
            temp.add(employee);
            if (employee.getSalary() < 450) {
                if (mapSalary.get(SalaryCategory.LOW) == null) {
                    mapSalary.put(SalaryCategory.LOW, temp);
                } else {
                    temp.addAll(mapSalary.get(SalaryCategory.LOW));
                    mapSalary.put(SalaryCategory.LOW, temp);
                }
            } else if (employee.getSalary() < 1100) {
                if (mapSalary.get(SalaryCategory.MEDIUM) == null) {
                    mapSalary.put(SalaryCategory.MEDIUM, temp);
                } else {
                    temp.addAll(mapSalary.get(SalaryCategory.MEDIUM));
                    mapSalary.put(SalaryCategory.MEDIUM, temp);
                }
            } else {
                if (mapSalary.get(SalaryCategory.HIGH) == null) {
                    mapSalary.put(SalaryCategory.HIGH, temp);
                } else {
                    temp.addAll(mapSalary.get(SalaryCategory.HIGH));
                    mapSalary.put(SalaryCategory.HIGH, temp);
                }
            }
        }
        for (SalaryCategory salaryCategory : mapSalary.keySet()) {
            System.out.println("Employees with " + salaryCategory + " salary");
            for (Employee employee : mapSalary.get(salaryCategory)) {
                System.out.println(employee);
            }
        }
    }
    static void task3() {
        Scanner scanner = null;
        try {
            File file = new File("/Users/Shared/CPP/CPP2Second.txt");
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        Set<Employee> set = new HashSet<Employee>();
        set.addAll(employees);
        while (scanner.hasNextLine()) {
            Employee temp = new Employee();
            String[] data = scanner.nextLine().split(" ");
            temp.setSurname(data[0]);
            temp.setPosition(data[1]);
            temp.setBirthYear(Integer.parseInt(data[2]));
            temp.setSalary(Integer.parseInt(data[3]));
            set.add(temp);
        }
        System.out.println("All employees");
        for (Employee employee : set) {
            System.out.println(employee);
        }
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Year: ");
        int year = input.nextInt();
        List<Employee> tempList = new ArrayList<Employee>(set);
        for (Employee employee : tempList) {
            if (year > employee.getBirthYear()) {
                set.remove(employee);
            }
        }
        tempList.clear();
        System.out.println("All employees after remove");
        for (Employee employee : set) {
            System.out.println(employee);
        }
        if (scanner != null) {
            scanner.close();
        }
    }
}
