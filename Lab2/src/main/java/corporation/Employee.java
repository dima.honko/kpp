package corporation;

public class Employee {
    private String surname;
    private String position;
    private int birthYear;
    private int Salary;

    public Employee(String surname, String position, int birthYear, int salary) {
        this.surname = surname;
        this.position = position;
        this.birthYear = birthYear;
        Salary = salary;
    }

    public Employee() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getSalary() {
        return Salary;
    }

    public void setSalary(int salary) {
        Salary = salary;
    }

    @Override
    public String toString() {
        return "surname= " + surname
            + ", position=" + position
            + ", birthYear=" + birthYear +
            ", Salary=" + Salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Employee employee = (Employee) o;

        if (birthYear != employee.birthYear) {
            return false;
        }
        if (Salary != employee.Salary) {
            return false;
        }
        if (surname != null ? !surname.equals(employee.surname) : employee.surname != null) {
            return false;
        }
        return position != null ? position.equals(employee.position) : employee.position == null;
    }

    @Override
    public int hashCode() {
        int result = surname != null ? surname.hashCode() : 0;
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + birthYear;
        result = 31 * result + Salary;
        return result;
    }
}
